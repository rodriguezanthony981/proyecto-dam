package com.example.project.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.example.project.R;

public class LoginActivity extends AppCompatActivity {

    EditText User, Password;
    Button BtnAceptar, BtnSalir, BtnCrearCta;

    CheckBox keepLoggedInCheckbox;
    public Boolean keepCheckBox = false;
    private SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setSupportActionBar(findViewById(R.id.toolbar));

        sharedPreferences = getSharedPreferences("my_preferences_file", MODE_PRIVATE);
        String username = sharedPreferences.getString("username", "");
        String password = sharedPreferences.getString("password", "");

        if (isLoggedIn()) {
            showMainScreen();
            Toast.makeText(LoginActivity.this, "Logging Skipped!", Toast.LENGTH_LONG).show();
        }

        User = findViewById(R.id.User);
        Password = findViewById(R.id.Password);
        BtnAceptar = findViewById(R.id.BtnAceptar);
        BtnSalir = findViewById(R.id.BtnSalir);
        BtnCrearCta = findViewById(R.id.BtnCrearCta);

        keepLoggedInCheckbox = findViewById(R.id.keep_logged_in_checkbox);

        BtnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validateCredentials(User.getText().toString(), Password.getText().toString())){
                    SharedPreferences sharedPreferences = getSharedPreferences("my_preferences_file", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();

                    if (keepCheckBox) {
                        editor.putString("username", User.getText().toString());
                        editor.putString("password", Password.getText().toString());
                        editor.apply();
                        keepCheckBox = true;
                        Toast.makeText(LoginActivity.this,"Datos: "+User.getText().toString()+", "+Password.getText().toString(), Toast.LENGTH_LONG).show();
                    } else {
                        editor.putString("username", "");
                        editor.putString("password", "");
                        editor.apply();
                    }

                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    intent.putExtra("Usuario", User.getText().toString());
                    startActivity(intent);
                    finish();

                } else {
                    Toast.makeText(LoginActivity.this, "Datos Incorrectos!", Toast.LENGTH_LONG).show();
                }

            }
        });

        BtnSalir.setOnClickListener(view -> finish());

        BtnCrearCta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, Crear_Cuenta.class);
                startActivity(intent);
                finish();
            }
        });

        keepLoggedInCheckbox.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                this.keepCheckBox = true;
                Toast.makeText(LoginActivity.this,"Mantener sesión: ON", Toast.LENGTH_LONG).show();
            } else {
                this.keepCheckBox = false;
                Toast.makeText(LoginActivity.this,"Mantener sesión: OFF", Toast.LENGTH_LONG).show();
            }
        });
    }
    private boolean validateCredentials(String username, String password) {
        ConexionDB dbHelper = new ConexionDB(this, "admin", null, 1);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        String selection = "Nombres = ? AND Password = ?";  // Modificación aquí
        String[] selectionArgs = {username, password};

        Cursor cursor = db.query("usuario", null, selection, selectionArgs, null, null, null);
        boolean loginSuccessful = cursor.moveToFirst();
        cursor.close();
        db.close();
        return loginSuccessful;
    }
    // Authenticator Login Skip
    private boolean isLoggedIn() {
        String username;
        String password;

        username = sharedPreferences.getString("username", "");
        password = sharedPreferences.getString("password", "");
        return validateCredentials(username, password);
    }

    private void showMainScreen() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    // END Authenticator

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.menu_option1) {
            Intent intent = new Intent(LoginActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
            return true;
        } else if (id == R.id.menu_option2) {
            // Acción para la opción 2
            return true;
        } else if (id == R.id.menu_about) {
            // Acción para la opción "Acerca de"
            Intent intent = new Intent(this, About.class);
            startActivity(intent);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}