package com.example.project.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.project.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class Settings extends AppCompatActivity {

    Button btnLoggout;
    TextView textView;
    String User;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        BottomNavigation();

        btnLoggout = findViewById(R.id.buttonloggout);

        btnLoggout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout();
            }
        });
        textView = findViewById(R.id.textViewUser);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String username = extras.getString("Usuario");
            User = username;
            textView.setText("Usuario: " + username);
        } else {
            sharedPreferences = getSharedPreferences("my_preferences_file", MODE_PRIVATE);
            String username = sharedPreferences.getString("username", "");
            User = username;
            textView.setText("Usuario: " + username);
        }
    }

    private void BottomNavigation() {
        FloatingActionButton floatingActionButton = findViewById(R.id.cartBtn);
        LinearLayout homeBtn = findViewById(R.id.home_Btn);
        LinearLayout settingBtn = findViewById(R.id.Setting_Btn);
        LinearLayout supportBtn = findViewById(R.id.Support_Btn);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Settings.this, CartListActivity.class));
                finish();
            }
        });

        homeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Settings.this, MainActivity.class);
                intent.putExtra("Usuario", User);
                startActivity(intent);
                finish();
            }
        });

        settingBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(Settings.this,"Estamos en Settings", Toast.LENGTH_LONG).show();
            }
        });

        supportBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Settings.this, About.class);
                intent.putExtra("Usuario", User);
                startActivity(intent);
                finish();
            }
        });
    }

    private void logout() {
        SharedPreferences sharedPreferences = getSharedPreferences("my_preferences_file", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.remove("username");
        editor.remove("password");
        editor.apply();

        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
        // Call this method when the user clicks the "Cerrar sesión" button
    }
}