package com.example.project.Activity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.project.R;

public class Crear_Cuenta extends AppCompatActivity implements CalendarFragment.OnDateSelectedListener{

    EditText Nombres, Apellidos, Cedula, Edad, EditTextDate, Password;
    Spinner Nacionalidad, Genero;
    RadioButton RdBtn_Soltero, RdBtn_Casado, RdBtn_Divorciado;
    private RadioGroup radioGroup;
    Button BtnRegistrar, BtnBorrar, BtnCancelar, btnSaveSD;
    public String nacionalidadSeleccionada, generoSeleccionado;
    private TextView mTextView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_cuenta);
        setSupportActionBar(findViewById(R.id.toolbar3));

        Nombres = findViewById(R.id.Nombres);
        Apellidos = findViewById(R.id.Apellidos);
        Cedula = findViewById(R.id.Cedula);
        Edad = findViewById(R.id.Edad);
        EditTextDate = findViewById(R.id.editTextDate);
        Password = findViewById(R.id.password);

        //Nacionalidad Spinner
        Nacionalidad = findViewById(R.id.Nacionalidad);
        ArrayAdapter<CharSequence> adapterN = ArrayAdapter.createFromResource(this,
                R.array.nacionalidades, android.R.layout.simple_spinner_item);
        adapterN.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        Nacionalidad.setAdapter(adapterN);
        Nacionalidad.setSelection(0); // Establece el elemento seleccionado por defecto
        //End Nacionalidad Spinner

        //Genero Spinner
        Genero = findViewById(R.id.Genero);
        ArrayAdapter<CharSequence> adapterG = ArrayAdapter.createFromResource(this,
                R.array.generos, android.R.layout.simple_spinner_item);
        adapterG.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Genero.setAdapter(adapterG);

        //Genero.setSelection(0); // Establece el elemento seleccionado por defecto
        //End Genero Spinner

        radioGroup = findViewById(R.id.thegroup);
        RdBtn_Soltero = findViewById(R.id.RdBtn_Soltero);
        RdBtn_Casado = findViewById(R.id.RdBtn_Casado);
        RdBtn_Divorciado = findViewById(R.id.RdBtn_Divorciado);
        BtnRegistrar = findViewById(R.id.BtnRegistrar);
        BtnBorrar = findViewById(R.id.BtnBorrar);
        BtnCancelar = findViewById(R.id.BtnCancelar);
        btnSaveSD = findViewById(R.id.btnSaveSD);

        ImageButton button = findViewById(R.id.imageButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog();
            }
        });

        btnSaveSD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Crear_Cuenta.this, Consulta.class);
                startActivity(intent);
            }
        });

        BtnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RegistroUsuarioDB(view);
            }
        });

        BtnBorrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Nombres.setText("");
                Apellidos.setText("");
                Cedula.setText("");
                Edad.setText("");
                EditTextDate.setText("");
                Password.setText("");
                radioGroup.clearCheck();
            }
        });

        BtnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Crear_Cuenta.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });

        Nacionalidad.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                nacionalidadSeleccionada = parent.getItemAtPosition(position).toString();
                //if (!nacionalidadSeleccionada.isEmpty()) {
                //    Toast.makeText(getApplicationContext(), "Nacionalidad seleccionada: " + nacionalidadSeleccionada, Toast.LENGTH_SHORT).show();
                //}
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Toast.makeText(getApplicationContext(), "Debe seleccionar una nacionalidad", Toast.LENGTH_SHORT).show();
            }
        });

        Genero.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                generoSeleccionado = parent.getItemAtPosition(position).toString();
                //if (!nacionalidadSeleccionada.isEmpty()) {
                //    Toast.makeText(getApplicationContext(), "Nacionalidad seleccionada: " + nacionalidadSeleccionada, Toast.LENGTH_SHORT).show();
                //}
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Toast.makeText(getApplicationContext(), "Debe seleccionar un genero", Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void RegistroUsuarioDB(View view){
        try {
            ConexionDB admin = new ConexionDB(this, "admin", null, 1);
            SQLiteDatabase db = admin.getWritableDatabase();

            String cedula = Cedula.getText().toString();
            String nombres = Nombres.getText().toString();
            String apellido = Apellidos.getText().toString();
            String edad = Edad.getText().toString();
            String fecha = EditTextDate.getText().toString();
            String password = Password.getText().toString();
            String nacionalidad = nacionalidadSeleccionada;
            String genero = generoSeleccionado;

            ContentValues registro = new ContentValues();

            registro.put("Cedula", cedula);
            registro.put("Nombres", nombres);
            registro.put("Apellidos", apellido);
            registro.put("Edad", edad);
            registro.put("EditTextDate", fecha);
            registro.put("Nacionalidad", nacionalidad);
            registro.put("Genero", genero);
            registro.put("Password", password);

            db.insert("usuario", null, registro);
            db.close();
            Toast.makeText(this, "Se realizo el registro con exito en la DB", Toast.LENGTH_SHORT).show();

        } catch (Exception e) {
            Toast.makeText(this, "Error al realizar el registro: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    //    private void guardar(String data) {
//        String fileName = "SaveData.txt";
//
//        try {
//            File file = new File(getExternalFilesDir(null), fileName);
//            FileWriter writer = new FileWriter(file, true);
//            writer.append(data + "\n");
//            writer.flush();
//            writer.close();
//            Toast.makeText(this, "Datos guardados", Toast.LENGTH_SHORT).show();
//        } catch (IOException e) {
//            e.printStackTrace();
//            Toast.makeText(this, "Error al guardar", Toast.LENGTH_SHORT).show();
//        }
//    }
    public void showDatePickerDialog() {
        CalendarFragment datePickerFragment = new CalendarFragment();
        datePickerFragment.setOnDateSelectedListener(this);
        datePickerFragment.show(getSupportFragmentManager(), "datePicker");
    }
    @Override
    public void onDateSelected(int year, int month, int day) {
        String selectedDate = String.format("%d-%02d-%02d", year, month + 1, day);
        EditTextDate.setText(selectedDate);
    }

}