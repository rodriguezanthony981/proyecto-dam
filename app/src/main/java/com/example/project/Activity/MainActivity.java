package com.example.project.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.project.Adaptor.PopluarAdaptor;
import com.example.project.Domain.FoodDomain;
import com.example.project.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private RecyclerView.Adapter adapter2, adapter3;
    private RecyclerView RecyclerViewPopularList, RecyclerViewBebidasList;
    TextView textView;
    String User;
    private SharedPreferences sharedPreferences;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerViewPopular();
        recyclerViewBebidas();
        BottomNavigation();
        textView = findViewById(R.id.textViewUser);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String username = extras.getString("Usuario");
            User = username;
            textView.setText("Bienvenido " + username);
        } else {
            sharedPreferences = getSharedPreferences("my_preferences_file", MODE_PRIVATE);
            String username = sharedPreferences.getString("username", "");
            User = username;
            textView.setText("Bienvenido " + username);
        }
    }

    private void BottomNavigation() {
        FloatingActionButton floatingActionButton = findViewById(R.id.cartBtn);
        LinearLayout homeBtn = findViewById(R.id.home_Btn);
        LinearLayout settingBtn = findViewById(R.id.Setting_Btn);
        LinearLayout supportBtn = findViewById(R.id.Support_Btn);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, CartListActivity.class));
                finish();
            }
        });

        homeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startActivity(new Intent(MainActivity.this, MainActivity.class));
                Toast.makeText(MainActivity.this,"Estamos en el Inicio", Toast.LENGTH_LONG).show();
            }
        });

        settingBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Settings.class);
                intent.putExtra("Usuario", User);
                startActivity(intent);
                finish();
            }
        });

        supportBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, About.class);
                intent.putExtra("Usuario", User);
                startActivity(intent);
                finish();
            }
        });

    }

    private void recyclerViewPopular() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        RecyclerViewPopularList = findViewById(R.id.recyclerView2);
        RecyclerViewPopularList.setLayoutManager(linearLayoutManager);

        ArrayList<FoodDomain> popularFood = new ArrayList<>();
        popularFood.add(new FoodDomain("Pizza", "pop_1", "pepperoni,mozzarella cheese, oregano, pasta de tomate, Embutidos, Aceitunas,Champiñones", 12.80));
        popularFood.add(new FoodDomain("Hamburguesa", "pop_2", "Carne,cheese,lechuga,tomate, huevo, tocino", 3.75));
        popularFood.add(new FoodDomain("Salchipapa", "salchipapa", "papas con salchicha", 2.5));
        popularFood.add(new FoodDomain("Pollo con papas", "papipollo", "papas con pollo (pechuga, ala, pierna)", 4.5));
        popularFood.add(new FoodDomain("Burritos", "burritos", "fríjoles, carne, queso, chiles, cebollas o especias.", 6.5));
        popularFood.add(new FoodDomain("Tacos", "tacos", "consiste en una tortilla (generalmente de maíz), seguido de un guiso, acompañado con cebolla, cilantro picado y limón, así como alguna salsa,", 7.5));

        adapter2 = new PopluarAdaptor(popularFood);
        RecyclerViewPopularList.setAdapter(adapter2);
    }

    private void recyclerViewBebidas() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        RecyclerViewBebidasList = findViewById(R.id.recyclerView3);
        RecyclerViewBebidasList.setLayoutManager(linearLayoutManager);

        ArrayList<FoodDomain> popularFood = new ArrayList<>();
        popularFood.add(new FoodDomain("Sodas", "sodas1", "Coca Cola - Sprite - Fanta", 1.75));
        popularFood.add(new FoodDomain("Jugo Natural", "jugo2", "Puede ser de: Naranja - Fresas - Limón", 1.50));
        popularFood.add(new FoodDomain("Batidos De Frutas", "batidos3", "Puede ser de: Mora - Fresas - Durazno", 0.80));
        popularFood.add(new FoodDomain("Botella de Agua", "bottled4", "Botella de 1 litro", 1.00));
        popularFood.add(new FoodDomain("Bebida Energética", "energia", "Puede ser de: Uva - Manzana - Mora Azul", 2.00));

        adapter3 = new PopluarAdaptor(popularFood);
        RecyclerViewBebidasList.setAdapter(adapter3);
    }
}