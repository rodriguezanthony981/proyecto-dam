package com.example.project.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.project.R;

public class Consulta extends AppCompatActivity {

    EditText Cedula, Nombres, Apellidos, Edad, EditTextDate, Genero, Nacionalidad, Password;
    Button btnBuscar, btnActualizar, btnEliminar, btnVolver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consulta);

        Cedula = findViewById(R.id.Cedula);
        Cedula.requestFocus();
        Nombres = findViewById(R.id.Nombres);
        Apellidos = findViewById(R.id.Apellidos);
        Edad = findViewById(R.id.Edad);
        EditTextDate = findViewById(R.id.editTextDate);
        Genero = findViewById(R.id.Genero);
        Nacionalidad = findViewById(R.id.Nacionalidad);
        Password = findViewById(R.id.password);

        btnBuscar = findViewById(R.id.btn_buscar);
        btnActualizar = findViewById(R.id.btn_actualizar);
        btnEliminar = findViewById(R.id.btn_eliminar);
        btnVolver = findViewById(R.id.btnVolver);

        btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buscarRegistro(view);
            }
        });

        btnVolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Consulta.this, Crear_Cuenta.class);
                startActivity(intent);
                finish();
            }
        });
        btnActualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actualizarRegistro();
            }
        });

        btnEliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eliminarRegistro();
            }
        });

        btnVolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Consulta.this, Crear_Cuenta.class);
                startActivity(intent);
                finish();
            }
        });
    }

    public void buscarRegistro(View view) {
        ConexionDB admin = new ConexionDB(this, "admin", null, 1);
        SQLiteDatabase db = admin.getReadableDatabase();
        String cedula = Cedula.getText().toString();
        String[] projection = {"Nombres", "Apellidos", "Edad", "EditTextDate", "Nacionalidad", "Genero", "Password"};
        String selection = "Cedula = ?";
        String[] selectionArgs = {cedula};
        Cursor cursor = db.query("usuario", projection, selection, selectionArgs, null, null, null);

        if (cursor.moveToFirst()) {
            String nombre = cursor.getString(cursor.getColumnIndexOrThrow("Nombres"));
            String apellido = cursor.getString(cursor.getColumnIndexOrThrow("Apellidos"));
            String edad = cursor.getString(cursor.getColumnIndexOrThrow("Edad"));
            String fecha = cursor.getString(cursor.getColumnIndexOrThrow("EditTextDate"));
            String nacionalidad = cursor.getString(cursor.getColumnIndexOrThrow("Nacionalidad"));
            String genero = cursor.getString(cursor.getColumnIndexOrThrow("Genero"));
            String password = cursor.getString(cursor.getColumnIndexOrThrow("Password"));

            Nombres.setText(nombre);
            Apellidos.setText(apellido);
            Edad.setText(edad);
            EditTextDate.setText(fecha);
            Nacionalidad.setText(nacionalidad);
            Genero.setText(genero);
            Password.setText(password);

        } else {
            Toast.makeText(this, "El usuario no existe", Toast.LENGTH_LONG).show();
        }
        cursor.close();
        db.close();
    }
    private void actualizarRegistro() {
        ConexionDB admin = new ConexionDB(this, "admin", null, 1);
        SQLiteDatabase db = admin.getWritableDatabase();

        String nombre = Nombres.getText().toString();
        String apellido = Apellidos.getText().toString();
        String edad = Edad.getText().toString();
        String fecha = EditTextDate.getText().toString();
        String nacionalidad = Nacionalidad.getText().toString();
        String password = Password.getText().toString();
        String genero = Genero.getText().toString();

        String[] cedula = {Cedula.getText().toString()};

        if(!nombre.isEmpty()){
            ContentValues registro = new ContentValues();

            registro.put("Nombres", nombre);
            registro.put("Apellidos", apellido);
            registro.put("Edad", edad);
            registro.put("EditTextDate", fecha);
            registro.put("Nacionalidad", nacionalidad);
            registro.put("Genero", genero);
            registro.put("Password", password);


            int dataR = db.update("usuario", registro, "Cedula=?", cedula );
            db.close();
            if(dataR == 1){
                Toast.makeText(this, "Se modificaron los datos ", Toast.LENGTH_LONG).show();
            }else{
                Toast.makeText(this, "El usuario no existe ", Toast.LENGTH_LONG).show();
            }
        } else{
            Toast.makeText(this, "no existe reservacion con ese codigo", Toast.LENGTH_LONG).show();
        }
    }

    private void eliminarRegistro() {
        Nombres.setText("");
        Apellidos.setText("");
        EditTextDate.setText("");
        Edad.setText("");
        Cedula.setText("");
    }
}